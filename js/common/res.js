var collectionedCourseArr = new Array();
var queryCollectionedCourseArrFlag = false;//查询收藏课程列表标识
var handleCourseId = "";
var handleClazzId = "";
var collectionFlag = true;//收藏标识
/**
 * 绑定课程确认框事件
 */
function bindCourseConfirmEvent(){
	$("a[data-name='yes']").click(function(){
		$('.cd-popup').removeClass('is-visible');
		if(collectionFlag){//收藏
			var url = ctxPath + "/res/subscribeCourse";
	    	var params = {"clazzId": handleClazzId};
	    	ajaxRequest(url, params, function(data){
				var status = data.status;
				if(status){//收藏成功
					collectionedCourseArr.push(handleCourseId);
					$("a[data-courseId='"+ handleCourseId +"'][data-name='collection-course']").addClass("current").html("已收藏");
					showMsg("收藏成功");
				}else{
					showMsg(data.msg);
				}
	    	}, function(){
	    		showMsg("收藏失败");
	    	});
		}else{
			var url = ctxPath + "/res/unSubscribeCourse";
	    	var params = {"clazzId": handleClazzId};
	    	ajaxRequest(url, params, function(data){
				var status = data.status;
				if(status){//取消收藏成功
					collectionedCourseArr.remove(handleCourseId);
					$("a[data-courseId='"+ handleCourseId +"'][data-name='collection-course']").removeClass("current").html("收藏");
					showMsg("取消收藏成功");
				}else{
					showMsg(data.msg);
				}
	    	}, function(){
	    		showMsg("取消收藏失败");
	    	});
		}
	});
	$("a[data-name='no']").click(function(){
		$('.cd-popup').removeClass('is-visible');
	});
}
/**封装图书
 * @param jsonObj
 * @returns {String}
 */
function packageBook(jsonObj){
	var url = jsonObj.url;
	url = specialTopicUrl.replace("param", jsonObj.id);
	var html = "<li data-name='item-book' data-courseId='"+jsonObj.id+"' data-courseName='"+jsonObj.name+"' "+ (isChaoxingApp() ? "" : "style='width:96%;margin-right:0;'") +">" +
					"<img src='"+ jsonObj.imageurl +"' />" +
					"<dl "+ (isChaoxingApp() ? "" : "style='width:70%;'") +">" +
						"<dt>"+ jsonObj.name +"</dt>" +
						"<dd>"+ jsonObj.teacherfactor +"</dd>" +
						"<dd>"+ (isEmpty(jsonObj.schools) ? '' : jsonObj.schools) +"</dd>" +
					"</dl>";
	if(isChaoxingApp()){
		html += "<a data-name='collection-book' href='javascript:void(0)' data-courseId='"+jsonObj.id+"' data-courseName='"+jsonObj.name+"' data-imageurl='"+jsonObj.imageurl+"' data-teacherfactor='"+jsonObj.teacherfactor+"' data-url='"+url+"'>收藏</a>";
	}
	html += "</li>";
	return html;
}
/**封装课程
 * @param data
 * @returns {String}
 */
function packageCourse(jsonObj){
	var clazz = jsonObj.clazz.data[0];
	var html = "<li data-name='item-course' data-courseId='"+jsonObj.id+"' data-clazzId='"+ clazz.id +"' data-bbsid='"+ jsonObj.bbsid +"' data-courseName='"+jsonObj.name+"' "+ (isChaoxingApp() ? "" : "style='width:96%;margin-right:0;'") +">" +
				"<img src='"+ jsonObj.imageurl +"' />" +
				"<dl "+ (isChaoxingApp() ? "" : "style='width:70%;'") +">" +
					"<dt>"+ jsonObj.name +"</dt>" +
					"<dd>"+ jsonObj.teacherfactor +"</dd>" +
					"<dd>"+ (isEmpty(jsonObj.schools) ? '' : jsonObj.schools) +"</dd>" +
				"</dl>";
	if(isChaoxingApp()){
		html += "<a data-name='collection-course' href='javascript:void(0)' data-courseId='"+jsonObj.id+"' data-clazzId='"+ clazz.id +"' data-bbsid='"+ clazz.bbsid +"' data-courseName='"+jsonObj.name+"' data-imageurl='"+jsonObj.imageurl+"' data-teacherfactor='"+jsonObj.teacherfactor+"'>收藏</a>";
	}
	html += "</li>";
	return html;
}
/**封装专题
 * @param data
 * @returns {String}
 */
function packageSpecialTopic(jsonObj){
	var url = jsonObj.url;
	url = specialTopicUrl.replace("param", jsonObj.id);
	var html = "<li data-name='item-specialTopic' data-courseId='"+jsonObj.id+"' data-courseName='"+jsonObj.name+"' "+ (isChaoxingApp() ? "" : "style='width:96%;margin-right:0;'") +">" +
					"<img src='"+ jsonObj.imageurl +"' />" +
					"<dl "+ (isChaoxingApp() ? "" : "style='width:70%;'") +">" +
						"<dt>"+ jsonObj.name +"</dt>" +
						"<dd>"+ jsonObj.teacherfactor +"</dd>" +
						"<dd>"+ (isEmpty(jsonObj.schools) ? '' : jsonObj.schools) +"</dd>" +
					"</dl>";
	if(isChaoxingApp()){
		html += "<a data-name='collection-specialTopic' href='javascript:void(0)' data-courseId='"+jsonObj.id+"' data-courseName='"+jsonObj.name+"' data-imageurl='"+jsonObj.imageurl+"' data-teacherfactor='"+jsonObj.teacherfactor+"' data-url='"+url+"'>收藏</a>";
	}
	html += "</li>";
	return html;
}
function bindBookEvent(){
	$("li[data-name='item-book']").off("click");
	$("li[data-name='item-book']").on("click", function(){
		var courseName = $(this).attr("data-courseName");
		var resId = $(this).attr("data-courseId");
		var url = specialTopicUrl.replace("param", resId);
		var opt = {"title":courseName, "checkLogin":"1", "loadType":"1" ,"webUrl":url,"toolbarType":"1","resId":resId};
		openSpecialTopic(opt);
	});
	$("a[data-name='collection-book']").off("click");
	$("a[data-name='collection-book']").on("click", function(event){
		if($(this).hasClass("current")){//已收藏则取消收藏
			var key = $(this).attr("data-courseId");
			cancelSubscribe(key);
		}else{//收藏
			var courseId = $(this).attr("data-courseId");
			var courseName = $(this).attr("data-courseName");
			var imageUrl = $(this).attr("data-imageurl");
			var teacherfactor = $(this).attr("data-teacherfactor");
			var url = specialTopicUrl.replace("param", courseId);
			var options = {
				"cataid" : cataObj.specialTopic.id,
				"cataName" : cataObj.specialTopic.name,
				"key" : "mooc_" + courseId,
				"content" : {
					"aid" : "mooc_" + courseId,
					"appid" : $.md5(url),
					"appname" : courseName,
					"appurl" : url,
					"cataid" : "100000001",
					"logopath" : imageUrl,
					"otherConfig" : {
						"author" : teacherfactor,
						"id" : "mooc_" + courseId
					}
				}
			};
			subscribe(options);
		}
		event.stopPropagation();//阻止冒泡
	});
}
function bindSpecialTopicEvent(){
	$("li[data-name='item-specialTopic']").off("click");
	$("li[data-name='item-specialTopic']").on("click", function(){
		var courseName = $(this).attr("data-courseName");
		var resId = $(this).attr("data-courseId");
		var url = specialTopicUrl.replace("param", resId);
		var opt = {"title":courseName, "checkLogin":"1", "loadType":"1" ,"webUrl":url,"toolbarType":"1","resId":resId};
		openSpecialTopic(opt);
	});
	$("a[data-name='collection-specialTopic']").off("click");
	$("a[data-name='collection-specialTopic']").on("click", function(event){
		if($(this).hasClass("current")){//已收藏则取消收藏
			var key = $(this).attr("data-courseId");
			cancelSubscribe(key);
		}else{//收藏
			var courseId = $(this).attr("data-courseId");
			var courseName = $(this).attr("data-courseName");
			var imageUrl = $(this).attr("data-imageurl");
			var teacherfactor = $(this).attr("data-teacherfactor");
			var url = specialTopicUrl.replace("param", courseId);
			var options = {
				"cataid" : cataObj.specialTopic.id,
				"cataName" : cataObj.specialTopic.name,
				"key" : "mooc_" + courseId,
				"content" : {
					"aid" : "mooc_" + courseId,
					"appid" : $.md5(url),
					"appname" : courseName,
					"appurl" : url,
					"cataid" : "100000001",
					"logopath" : imageUrl,
					"otherConfig" : {
						"author" : teacherfactor,
						"id" : "mooc_" + courseId
					}
				}
			};
			subscribe(options);
		}
		event.stopPropagation();//阻止冒泡
	});
}
function bindCourseEvent(){
	$("li[data-name='item-course']").off("click");
	$("li[data-name='item-course']").on("click", function(){
		var clazzId = $(this).attr("data-clazzId");
		var courseId = $(this).attr("data-courseId");
		var courseName = $(this).attr("data-courseName");
		var teacherfactor = $(this).attr("data-teacherfactor");
		var imageurl = $(this).attr("data-imageurl");
		var bbsid = $(this).attr("data-bbsid");
		openCourse(clazzId, courseId, courseName, teacherfactor, imageurl, bbsid);
	});
	$("a[data-name='collection-course']").off("click");
	$("a[data-name='collection-course']").on("click", function(event){
		var clazzId = $(this).attr("data-clazzId");
		var courseId = $(this).attr("data-courseId");
		if($(this).hasClass("current")){//已收藏则取消收藏
			unSubscribeCourse(clazzId, courseId);
		}else{//收藏
			subscribeCourse(clazzId, courseId);
		}
		event.stopPropagation();//阻止冒泡
	});
}
/**设置图书专题收藏状态
 * @param list
 */
function setBookSpecialTopicCollectionStatus(list){
	$(list).each(function(i){
		var resType = this.resType;
		if("course" != resType){
			var appid = this.id + "";
			if (appid.indexOf("mooc_") == -1) {
				appid = "mooc_" + appid;
			}
			jsBridge.postNotification('CLIENT_RES_SUBSCRIPTION_STATUS', {
				"cataid": cataObj.specialTopic.id,
	        	"cataName": cataObj.specialTopic.name,
	        	"key":appid
			});
		}
	});
}
/******************************************************专题图书相关******************************************************/
/**查询收藏状态的回调
 * @param object
 */
function subscriptionStatusCallback(object){
	var status = object.status;
	var key = object.key;
	key = key.replace("mooc_", "").replace("yp_", "");
	if("1" == status){//已收藏
		$("a[data-courseId='"+ key +"'][data-name='collection-book']").addClass("current").html("已收藏");
		$("a[data-courseId='"+ key +"'][data-name='collection-specialTopic']").addClass("current").html("已收藏");
	}else{//未收藏
		$("a[data-courseId='"+ key +"'][data-name='collection-book']").removeClass("current").html("收藏");
		$("a[data-courseId='"+ key +"'][data-name='collection-specialTopic']").removeClass("current").html("收藏");
	}
}
/**收藏的回调
 * @param object
 */
function subscriptionCallback(object){
	var status = object.status;
	var key = object.key;
	if("1" == status){
		key = key.replace("mooc_", "").replace("yp_", "");
		showMsg("收藏成功");
		$("a[data-courseId='"+ key +"'][data-name='collection-book']").addClass("current").html("已收藏");
		$("a[data-courseId='"+ key +"'][data-name='collection-specialTopic']").addClass("current").html("已收藏");
	}else{
		showMsg("收藏失败,请稍后重试");
	}
}
/**取消收藏的回调
 * @param object
 */
function cancelSubscriptionCallback(object){
	var status = object.status;
	var key = object.key;
	if("1" == status){
		key = key.replace("mooc_", "").replace("yp_", "");
		$("a[data-courseId='"+ key +"'][data-name='collection-book']").removeClass("current").html("收藏");
		$("a[data-courseId='"+ key +"'][data-name='collection-specialTopic']").removeClass("current").html("收藏");
		showMsg("取消收藏成功");
	}else{//专题分为mooc专题和云盘专题,mooc专题需要添加mooc_前缀,云盘专题需要添加yp_前缀
		showMsg("取消收藏失败,请稍后重试");
	}
}
/******************************************************** 课程相关 *******************************************************/
/**
 * 查询收藏的课程列表
 */
function querySubscribedCourses(){
	var url = ctxPath + "/res/querySubscribeds";
	var param = {"uid": getUid()};
	ajaxRequest(url, param, function(data){
		data = data.data[0].clazz.data;
		if(queryCollectionedCourseArrFlag){
			return;
		}
		if(data != null && data.length > 0){
			queryCollectionedCourseArrFlag = true;
			$(data).each(function(i){
				var course = this.course.data[0];
				var courseId = course.id;
				collectionedCourseArr.push(courseId);
			});
			hanleCourseCollectionStatus();
		}
	}, function(){
		//不做处理
	});
}
/**
 * 处理订阅的课程显示方式
 */
function hanleCourseCollectionStatus(){
	if(!queryCollectionedCourseArrFlag){
		querySubscribedCourses();
	}else{
		$(collectionedCourseArr).each(function(){
			$("a[data-courseId='"+ this +"'][data-name='collection-course']").addClass("current").html("已收藏");
		});
	}
}
/**收藏课程
 * @param clazzId
 * @param courseId
 */
function subscribeCourse(clazzId, courseId){
	collectionFlag = true;
	$("#collection-name").html("确认收藏?");
	handleClazzId = clazzId;
	handleCourseId = courseId;
	$('.cd-popup').addClass('is-visible');
}
/**取消课程的收藏
 * @param clazzId
 * @param courseId
 */
function unSubscribeCourse(clazzId, courseId){
	collectionFlag = false;
	$("#collection-name").html("取消收藏?");
	handleClazzId = clazzId;
	handleCourseId = courseId;
	$('.cd-popup').addClass('is-visible');
}