﻿//var staticResourceUrl = "http://static.basicedu.chaoxing.com";
var origin = window.location.origin;
var specialTopicUrl = "http://zhuanti.chaoxing.com/mobile/mooc/tocourse/param";
var courseUrl = "http://mooc1-api.chaoxing.com/phone/courseindex?courseid=param";
var cookie_domainId_name = "cookie_domainId";
var cookie_domainSearchFlag_name = "cookie_domainSearchFlag";
var search_his_key = "search_his";
var ctxPath = "/xnhy";
$(function() {
	try {
		FastClick.attach(document.body);//由于tap的点透问题而使用fastclick来替换
	} catch (e) {
	}
});
/**是否为空
 * @param str
 * @returns {Boolean}
 */
function isEmpty(str){
	return typeof(str) == undefined || typeof(str) == "undefined" || str == null || "" == $.trim(str);
}
/**
 * 显示加载遮罩层
 */
function showLoading() {
	
}
/**根据json字符串获取json
 * @param jsonStr
 */
function getJson(jsonStr){
	if(isEmpty(jsonStr)){
		return null;
	}
	return JSON.parse(jsonStr);
}
/**根据json获取json字符串
 * @param json
 * @returns
 */
function getJsonStr(json){
	if(isEmpty(json)){
		return null;
	}
	return JSON.stringify(json);
}
/**验证方法是否存在
 * @param functionName
 */
function isFunctionExist(functionName){
	return !isEmpty(functionName) && typeof(functionName) == "function";
}
/**ajax提交
 * @param url
 * @param type
 * @param params
 * @param success
 * @param error
 */
function ajaxRequest(url, params, success, error){
	$.ajax({
		url: url,
		type: "post",
		data: params,
		dataType: "json",
		success: function(response){
			eval("("+ success + "("+ response +")" +")");
		},
		error:function(){
			eval("("+ error + "())");
		}
	});
}
/**
 * 到顶部
 */
function gotop() {
	toTop('top', false);
}
/** 资源 */
var resourceObj = {
	"course": "0",//课程
	"specialTopic": "1"//专题
};
var cataObj = {
	"course": {"id": "100000002", "name": "课程"},//课程
	"specialTopic": {"id": "100000001", "name": "专题"}//专题
};
/**
 * 获取请求当前页面的参数
 * @param name
 * @returns
 */
function getUrlX(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
/**显示提示信息信息
 * @param msg
 */
function showMsg(msg){
	if(isChaoxingApp()){
		jsBridge.postNotification('CLIENT_DISPLAY_MESSAGE', {
			"message" : msg
		});
	}else{
		alert(msg);
	}
}
/**
 * 关闭加载遮罩层
 */
function closeLoading(){
	//layer.closeAll();
}
/**
 * 交互js初始化完成后的回调
 */
function _jsBridgeReady(){
	//获取用户信息的回调
	jsBridge.bind('CLIENT_GET_USERINFO', function(object) {
		if (typeof (object) != "undefined") {
			//此处使用约定的回调方法callback
			if(typeof(getUserInfoCallback) != undefined && 
					typeof(getUserInfoCallback) != "undefined" && 
					typeof(getUserInfoCallback) == "function"){
				getUserInfoCallback(object);
			}
		}
	});
	//绑定登录验证回调
	jsBridge.bind('CLIENT_LOGIN_STATUS', function(object) {
		if (typeof (object) != "undefined") {
			var status = object.status;
			//此处使用约定的回调方法callback
			if(typeof(loginStatusCallback) != undefined && 
					typeof(loginStatusCallback) != "undefined" && 
					typeof(loginStatusCallback) == "function"){
				loginStatusCallback(status);
			}
		}
	});
	//绑定登录回调
	jsBridge.bind('CLIENT_LOGIN', function(object) {
		if (typeof (object) != "undefined") {
			var status = object.status;
			//此处使用约定的回调方法callback
			if(typeof(loginCallback) != undefined && 
					typeof(loginCallback) != "undefined" && 
					typeof(loginCallback) == "function"){
				loginCallback(status);
			}
		}
	});
	//绑定获取订阅状态回调
	jsBridge.bind('CLIENT_RES_SUBSCRIPTION_STATUS', function(object) {
		if (typeof (object) != "undefined") {
			//此处使用约定的回调方法callback
			if(typeof(subscriptionStatusCallback) != undefined && 
					typeof(subscriptionStatusCallback) != "undefined" && 
					typeof(subscriptionStatusCallback) == "function"){
				subscriptionStatusCallback(object);
			}
		}
	});
	//订阅回调
	jsBridge.bind('CLIENT_SUBSCRIBE_RES', function(object) {
		if (typeof (object) != "undefined") {
			//此处使用约定的回调方法callback
			if(typeof(subscriptionCallback) != undefined && 
					typeof(subscriptionCallback) != "undefined" && 
					typeof(subscriptionCallback) == "function"){
				subscriptionCallback(object);
			}
		}
	});
	//取消订阅回调
	jsBridge.bind('CLIENT_REMOVE_RES', function(object) {
		if (typeof (object) != "undefined") {
			//此处使用约定的回调方法callback
			if(typeof(cancelSubscriptionCallback) != undefined && 
					typeof(cancelSubscriptionCallback) != "undefined" && 
					typeof(cancelSubscriptionCallback) == "function"){
				cancelSubscriptionCallback(object);
			}
		}
	});
	if(typeof(ready) != undefined && 
			typeof(ready) != "undefined" && 
			typeof(ready) == "function"){
		ready();
	}
}

/**
 * 去书架
 */
function goBookshelf(){
	var content = {
		"accountKey": "",
		"aid": "2108",
		"appid": "tushu",
		"appname": "书架",
		"appurl": "",
		"available": "1",
		"bind": "1",
		"cataid": "0",
		"description": "",
		"extendField": "",
		"focus": "0",
		"id": "2108",
		"isPrivate": "1",
		"isWebapp": "0",
		"loginId": "0",
		"logopath": "http://img1.16q.cn/ab5a8d921470c687c19e380a6a395905?w={WIDTH}&h={HEIGHT}",
		"needLogin": "0",
		"needRegist": "0",
		"norder": "15",
		"properties": "",
		"useClientTool": "1"
	};
	jsBridge.postNotification('CLIENT_OPEN_RES', {"cataid":0,"key":"2108","content": content,"cataName":"书架"});
}
/**打开课程
 * @param clazzId 班级ID
 * @param courseId 课程ID
 * @param courseName 课程名称
 * @param teacherfactor 
 * @param imageurl 封面地址
 * @param bbsid 讨论组ID
 */
function openCourse(clazzId, courseId, courseName, teacherfactor, imageurl, bbsid){
	if(isChaoxingApp()){
		var opts = {"bbsid": bbsid,"course": {"data": [{"id": courseId,"imageurl": imageurl,"name": courseName,"teacherfactor": teacherfactor}]},"id": clazzId};
		jsBridge.postNotification('CLIENT_OPEN_RES',  {"cataid":cataObj.course.id,"key":clazzId,"content": opts,"cataName":cataObj.course.name});
	}else{
		window.location.href = courseUrl.replace("param", courseId);
	}
}
/**打开专题
 * @param courseName 专题名称
 * @param url 专题url
 */
function openSpecialTopic(opts){
	if(isChaoxingApp()){
		jsBridge.postNotification('CLIENT_OPEN_URL', opts);
	}else{
		window.location.href = specialTopicUrl.replace("param", opts.resId);
	}
}
/**订阅
 * @param options
 */
function subscribe(options){
	jsBridge.postNotification("CLIENT_SUBSCRIBE_RES", $.extend({
			"cataid" : 100000001,
			"cataName" : "专题",
			"key" : "",
			"content" : {
				"accountKey" : "",
				"aid" : "",
				"appid" : "",
				"appname" : "",
				"appurl" : "",
				"available" : 1,
				"bind" : 1,
				"cataid" : "100000001",
				"clientType" : 127,
				"description" : "",
				"focus" : 0,
				"id" : -1,
				"isPrivate" : 1,
				"isWebapp" : 1,
				"loginId" : 0,
				"loginUrl" : "",
				"logoPath" : "",
				"logopath" : "",
				"logoshowtype" : 1,
				"needLogin" : 0,
				"needRegist" : 0,
				"norder" : 2147483647,
				"otherConfig" : {
					"author" : "",
					"id" : ""
				},
				"productId" : 3,
				"properties" : "",
				"rights" : 1,
				"usable" : "",
				"useClientTool" : 2
			}
		}, options));
}
/**取消订阅
 * @param key
 */
function cancelSubscribe(key){
	jsBridge.postNotification("CLIENT_REMOVE_RES",{
		cataid: cataObj.specialTopic.id,
		cataName: cataObj.specialTopic.name,
		key: "mooc_" + key
	});
}
/**获取cookie的值
 * @param name
 * @returns
 */
function getCookie(name) {
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
	if(arr = document.cookie.match(reg)){
		return unescape(arr[2]);
	}else{
		return null;
	}
}
/**获取app登录用户id
 * @returns
 */
function getUid(){
	return getCookie("_uid");
}
/**打开url
 * @param title
 * @param url
 */
function openUrl(title, url){
	if(isChaoxingApp()){
		var opts = {"title":title, "checkLogin":"1", "loadType":"1" ,"webUrl":url, "toolbarType":"1"};
		jsBridge.postNotification('CLIENT_OPEN_URL', opts);
	}else{
		window.location.href = url;
	}
}
Array.prototype.remove = function(val) {
	var index = this.indexOf(val);
	if (index > -1) {
		this.splice(index, 1);
	}
};
/**
 * 到搜索资源页面
 */
function searchBook() {
	var url = origin + ctxPath + "/search/toSearch";
	openUrl("搜索", url);
}
/**将domianId设置到cookie里面
 * @param domainId
 */
function setDomainIdToCookie(domainId){
	setCookie(cookie_domainId_name, domainId);
}
/**从cookie中获取domainId
 * @returns
 */
function getDomainIdFromCookie(){
	return getCookie(cookie_domainId_name);
}
/**将域搜索标识设置到cookie里面
 * @param domainSearchFlag
 */
function setDomainSearchFlagToCookie(domainSearchFlag){
	setCookie(cookie_domainSearchFlag_name, domainSearchFlag);
}
/**从cookie中获取域搜索标识
 * @returns
 */
function getDomainSearchFlagFromCookie(){
	return getCookie(cookie_domainSearchFlag_name);
}
/**设置cookie
 * @param name
 * @param value
 */
function setCookie(name, value){
	var Days = 30;
	var exp = new Date();
	exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}
/**获取cookie的值
 * @param name
 */
function getCookie(name){
	var arr;
	var reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
	if (arr = document.cookie.match(reg)){
		return unescape(arr[2]);
	}
	return null;
}
/**
 * 是否可搜索
 */
function isSearchAble(){
	return "1" == getDomainSearchFlagFromCookie();
}
var browser = {
	versions : function() {
		var u = navigator.userAgent, app = navigator.appVersion;
		return { //移动终端浏览器版本信息  
			trident : u.indexOf('Trident') > -1, //IE内核  
			presto : u.indexOf('Presto') > -1, //opera内核  
			webKit : u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核  
			gecko : u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核  
			mobile : !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端  
			ios : !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端  
			android : u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器  
			iPhone : u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器  
			iPad : u.indexOf('iPad') > -1, //是否iPad  
			webApp : u.indexOf('Safari') == -1
		//是否web应该程序，没有头部与底部  
		};
	}(),
	language : (navigator.browserLanguage || navigator.language).toLowerCase()
}
/**判断是不是移动端
 * @returns
 */
function isMobile(){
	return browser.versions.mobile;
}
/**是否是超星客户端
 * @returns {Boolean}
 */
function isChaoxingApp(){
	var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
	if (ua.match(/ChaoXingStudy/i) == "chaoxingstudy") {
		return true;
	}
	return false;
}
/**是否是微信客户端
 * @returns {Boolean}
 */
function isWeiXin(){
	if(browser.versions.mobile){//判断是否是移动设备打开。browser代码在下面  
		var ua = navigator.userAgent.toLowerCase();//获取判断用的对象  
		if(ua.match(/MicroMessenger/i) == 'micromessenger') {
			return true;
		}
	}
	return false;
}
/**绑定回到顶部
 * @param id
 */
function bindScrollToTop(id){
	$(window).scroll(function(){  
		if($(document).scrollTop() > 200){
			$("#" + id).fadeIn(1000);
		}else{
			$("#" + id).fadeOut(1000);
		} 
    }); 
	$("#" + id).click(function(){  
        $('body,html').animate({scrollTop:0},500);  
        return false;  
    });
}